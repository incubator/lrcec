# Oui, Les Réunions, C'Est Chiant !

LRCEC est là pour que tout le monde puisse s'en rendre compte et le dire
haut et fort pendant les réunions.

L'outil permet de recueillir rapidement les ressentis de manière silencieuse, pour chaque sujet d'une réunion. Il permet également de voter si besoin.

Les ressentis sont retranscrits sur un "foutaisomètre" sous la forme d'une jauge visible sur l'écran de l'animateur
et des participants.

## Démo

*  Version web : https://lrcec.demo.codelutin.com
*  APK pour Android : https://gitlab.nuiton.org/incubator/lrcec/-/wikis/uploads/923f465d0e5aeaaeb36b00052085c9af/lrcec-release.apk

# À faire

 * persistence et/ou purge des réunions passées ?

