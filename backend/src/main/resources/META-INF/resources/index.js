// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
// and then call `Vue.use(VueRouter)`.

const baseUrl = '/api';


// 1. Define route components.
// These can be imported from other files
const Home = {
  data: function () {
    return {
      meetingId: null,
      errorMessage: null
    };
  },
  template:
  `<div>
    <h3>Les Réunions C'Est Chiant</h3>
    <button v-on:click="newMeeting" class="btn btn-primary">Emmerder les autres</button>
    <hr/>
    <div class="form-inline">
      <div class="form-group">
        <input type="text" name="id" placeholder="RNox0h" v-model="meetingId" class="form-control" />
        <button v-on:click="joinMeeting" class="btn btn-primary" v-bind:disabled="!meetingId">Je suis maso</button>
      </div>
    </div>
    <p v-html="errorMessage" class="text-danger"></p>
  </div>`,
  methods: {
    newMeeting: function () {
      console.log("Create a new meeting, NOW!");
      let url = baseUrl + '/meeting';
      this.$http.post(url).then(response => {
        let meetingId = response.body;
        router.push({ path: `/m/${meetingId}` })
      }, response => {
        console.error(response);
      });
    },
    joinMeeting: function () {
      this.errorMessage = null;
      console.log("Joining  meeting, NOW!");
      let url = baseUrl + '/meeting/' + this.meetingId;
      this.$http.get(url).then(response => {
        console.log(response);
        if (response.status == 200) {
          router.push({ path: `/m/${this.meetingId}/current` });
        } else {
          this.errorMessage = `La réunion ${this.meetingId} n'existe pas`;
        }
      }, response => {
        console.error(response);
        if (response.status == 404) {
          this.errorMessage = `La réunion ${this.meetingId} n'existe pas`;
        } else {
          this.errorMessage = `Impossible de rejoindre la réunion ${this.meetingId}`;
        }
      });
    }
  }
}

const SubjectAdmin = {
  props: ['subject'],
  data: function () {
    return {
    };
  },
  template:
  `<div>
    <h4>Sujet pénible en cours : {{subject.title}}</h4>
    <div v-if="subject.feelings">
      Foutaisomètre :
      <v-gauge :value="subject.bullshitLevel"></v-gauge>
      <br/>
      Feelings :
      <ul>
        <li v-for="feeling in subject.feelings" v-html="feeling"></li>
      </ul>
      <br/>
      Votes :
      <ul>
        <li v-for="vote in Object.keys(subject.votesCount)">{{vote}} {{subject.votesCount[vote]}}</li>
      </ul>
    </div>
  </div>`
};
Vue.component('subject-admin', SubjectAdmin);

const Meeting = {
  props: ['id'],
  data: function () {
    return {
      subjectsFinished: [],
      newSubjectTitle: null,
      currentSubject: null,
      errorMessage: null
    };
  },
  template:
  `<div>
    <h3>Réunion chiante {{id}} en cours </h3>
    <h2 v-html="errorMessage" class="text-danger"></h2>
    <hr/>
    <form class="form-inline" v-if="!errorMessage">
      <div class="form-group">
        <input type="text" v-model="newSubjectTitle" placeholder="Les forfaits téléphoniques" class="form-control"></input>
        <button v-on:click="newSubject" class="btn btn-primary" v-bind:disabled="!newSubjectTitle">Rien que pour les faire chier ...</button>
      </div>
    </form>
    <div v-if="currentSubject">
      <hr/>
      <subject-admin v-bind:subject="currentSubject"></subject-admin>
    </div>
    <div v-if="subjectsFinished.length > 0">
      <hr/>
      <h4>Sujets pénibles terminés :</h4>
      <ul>
        <li v-for="subject in subjectsFinished">
            <span v-html="subject.title"></span>
            <span v-if="subject.duration" style="color: grey; font-style: italic;">({{subject.duration}} secondes de souffrance)</span>
        </li>
      </ul>
    </div>
  </div>`,
  created: function () {
    this.reload();

    // Compute websocket URL
    let wsProtocol = "ws:";
    if (location.protocol == "https:") {
      wsProtocol = "wss:";
    }
    let wsUrl = wsProtocol + "//" + location.host + `/api/meeting/${this.id}/watcherWS`;
    console.log("About to open websocket to " + wsUrl);

    // Open websocket
    let ws = new WebSocket(wsUrl);
    ws.onmessage = function() {
      this.reload();
    }.bind(this);
  },
  methods: {
    newSubject: function () {
      console.log("Create a new subject, NOW!");
      let url = baseUrl + '/meeting/' + this.id + '/subject';
      let options = { headers: { "Content-Type": 'text/plain' } };
      this.$http.post(url, this.newSubjectTitle, options).then(response => {
        let subjectId = response.body;
        console.log("Sujet créé: " + subjectId);
        this.reload();
        this.newSubjectTitle = '';
      }, response => {
        console.error(response);
      });
    },
    reload: function() {
      console.log("Need to display meeting " + this.$route.params.id);
      let meetingId = this.$route.params.id;
      var url = baseUrl + '/meeting/' + meetingId;
      this.$http.get(url).then(response => {
        let meeting = response.body;
        this.meetingLoaded(meeting);
      }, response => {
        if (response && response.status == 404) {
          this.errorMessage = 'La réunion n\'existe pas';
          console.log(this.errorMessage);
        } else {
          console.error(response);
        }
      });
  
    },
    meetingLoaded: function(meeting) {
      let subjects = meeting.subjects;
      this.subjectsFinished = [];
      if (subjects && meeting.currentSubjectId) {
        subjects.forEach(element => {
          if (element.id == meeting.currentSubjectId) {
            this.currentSubject = element;
          }
          else {
            this.subjectsFinished.push(element);
          }
        });
      }
    }
  }
}
const Subject = {
  props: ['id'],
  data: function () {
    return {
      subject: null,
      possibleFeelings: [],
      feeling: null,
      possibleVotes: [],
      vote: null
    };
  },
  template:
  `<div>
    <h3>Réunion chiante {{id}}</h3>
    <hr/>
    <span v-if="!subject" style="color: grey; font-style: italic;">
      Pas encore de sujet pénible ...
    </span>
    <div v-if="subject">
      <h4>Sujet pénible en cours depuis {{subject.duration}} secondes : {{subject.title}}</h4>
      <hr/>
      <v-gauge :value="subject.bullshitLevel"></v-gauge>
      <label for='feeling'>Feeling</label>
      <button v-on:click="feelingClicked(option)"
              v-for="option in possibleFeelings"
              v-bind:disabled="option == feeling"
              class="btn btn-primary choice">
        {{option}}
      </button>
      <br/>
      <label for='vote'>Vote</label>
      <button v-on:click="voteClicked(option)"
              v-for="option in possibleVotes"
              v-bind:disabled="option == vote"
              class="btn btn-primary choice">
        {{option}}
      </button>
    </div>
  </div>`,
  created: function () {

    console.log("Need to display meeting " + this.id);
    let meetingId = this.id;

    this.$http.get(baseUrl + `/meeting/${meetingId}/possibleFeelings`).then(response => {
      this.possibleFeelings = response.body;
    });

    this.$http.get(baseUrl + `/meeting/${meetingId}/possibleVotes`).then(response => {
      this.possibleVotes = response.body;
    });

    this.loadSubject('current');

    // Compute websocket URL
    let wsProtocol = "ws:";
    if (location.protocol == "https:") {
      wsProtocol = "wss:";
    }
    let wsUrl = wsProtocol + "//" + location.host + `/api/meeting/${this.id}/subject/currentWS`;
    console.log("About to open websocket to " + wsUrl);

    // Open websocket
    let ws = new WebSocket(wsUrl);
    ws.onmessage = function(m) {
        let subjectId = m.data;
        console.log("Switching subject: " + m.data);
        this.loadSubject(subjectId)
    }.bind(this);

    setInterval(this.incrementDuration, 1000);
  },
  methods: {
    incrementDuration: function() {
      if (this.subject) {
        this.subject.duration++;
      }
    },
    loadSubject: function(subjectId) {
      let url = baseUrl + `/meeting/${this.id}/subject/${subjectId}`;
      this.$http.get(url).then(response => {
        this.subject = response.body;
        this.feeling = this.subject.feeling;
        this.vote = this.subject.vote;
      }, response => {
        console.error(response);
      });
    },
    feelingChanged: function(evt) {
      let url = baseUrl + `/meeting/${this.id}/subject/${this.subject.id}/feeling`;
      let options = { headers: { "Content-Type": 'text/plain' } };
      this.$http.post(url, this.feeling, options);
    },
    feelingClicked: function(option) {
      this.feeling = option;
      this.feelingChanged();
    },
    voteChanged: function(evt) {
      let url = baseUrl + `/meeting/${this.id}/subject/${this.subject.id}/vote`;
      let options = { headers: { "Content-Type": 'text/plain' } };
      this.$http.post(url, this.vote, options);
    },
    voteClicked: function(option) {
      this.vote = option;
      this.voteChanged();
    }
  }
}

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/', component: Home },
  { path: '/m/:id', component: Meeting, props: true },
  { path: '/m/:id/current', component: Subject, props: true }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
})

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
  router
}).$mount('#app')

// Now the app has started!
