package org.chorem.lrcec.service;

import io.quarkus.arc.config.ConfigProperties;

@ConfigProperties(prefix = "lrcec")
public interface LrcecConfig {

    int getRetentionMinutes();

}
