package org.chorem.lrcec.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class MeetingService {

    private Cache<String, Meeting> meetings;

    @Inject
    protected LrcecConfig config;

    private Cache<String, Meeting> getMeetings() {
        if (meetings == null) {
            int retentionMinutes = config.getRetentionMinutes();
            System.out.println("Retention minutes : " + retentionMinutes);
            meetings = CacheBuilder.newBuilder()
                    .expireAfterAccess(retentionMinutes, TimeUnit.MINUTES)
                    .removalListener(notification -> System.out.println("Suppression de la réunion " + notification.getKey()))
                    .build();
        }
        return meetings;
    }

    protected String pickNewId() {
        UUID uuid = UUID.randomUUID();
        String hashed = Hashing.crc32().hashString(uuid.toString(), Charset.defaultCharset()).toString();
        String cutted = hashed.substring(0, 4).toUpperCase();
        if (getMeetings().getIfPresent(cutted) != null) {
            return pickNewId();
        }
        return cutted;
    }

    public String create() {
        String meetingId = pickNewId();

        Meeting meeting = new Meeting();
        getMeetings().put(meetingId, meeting);
        return meetingId;
    }

    public String newSubject(String meetingId, String title) {
        Meeting meeting = getMeetings().getIfPresent(meetingId);

        LocalDateTime now = LocalDateTime.now();

        meeting.currentSubjectId
                .flatMap(meeting::findSubject)
                .ifPresent(subject -> subject.finishedAt = Optional.of(now));

        String newSubjectId = String.valueOf(meeting.subjects.size());

        Subject newSubject = new Subject();
        newSubject.id = newSubjectId;
        newSubject.title = title;
        newSubject.startedAt = now;

        meeting.subjects.add(newSubject);

        meeting.currentSubjectId = Optional.of(newSubjectId);
        meeting.version++;

        return newSubjectId;
    }

    public void newVote(String meetingId, String subjectId, Vote vote, String userId) {
        Meeting meeting = getMeetings().getIfPresent(meetingId);
        meeting.findSubject(subjectId)
                .ifPresent(subject -> {
                    subject.votes.put(userId, vote);
                    subject.computeValues();
                });
        meeting.version++;
    }

    public void newFeeling(String meetingId, String subjectId, Feeling feeling, String userId) {
        Meeting meeting = getMeetings().getIfPresent(meetingId);
        meeting.findSubject(subjectId)
                .ifPresent(subject -> {
                    subject.feelings.put(userId, feeling);
                    subject.computeValues();
                });
        meeting.version++;
    }

    public Meeting getMeeting(String meetingId) {
        Meeting result = getMeetings().getIfPresent(meetingId);
        return result;
    }

    public Map<String, Meeting> list() {
        ImmutableMap<String, Meeting> result = ImmutableMap.copyOf(getMeetings().asMap());
        return result;
    }

    public Subject getCurrentSubject(String meetingId) {
        Meeting meeting = getMeetings().getIfPresent(meetingId);
        Subject result = meeting.findCurrentSubject()
                .orElse(null);
        return result;
    }

    public MySubject getCurrentSubjectForMe(String meetingId, String userId) {
        Meeting meeting = getMeetings().getIfPresent(meetingId);
        MySubject result = meeting.findCurrentSubject()
                .map(subject -> subject.forMe(userId))
                .orElse(null);
        return result;
    }

    public MySubject getSubject(String meetingId, String subjectId, String userId) {
        Meeting meeting = getMeetings().getIfPresent(meetingId);
        MySubject result = meeting.findSubject(subjectId)
                .map(subject -> subject.forMe(userId))
                .orElse(null);
        return result;
    }

}
