package org.chorem.lrcec.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Meeting {

    long version = 0L;

    List<Subject> subjects = new LinkedList<>();

    Optional<String> currentSubjectId = Optional.empty();

    Optional<Subject> findSubject(String subjectId) {
        Optional<Subject> result = subjects
                .stream()
                .filter(subject -> subjectId.equals(subject.id))
                .findAny();
        return result;
    }

    Optional<Subject> findCurrentSubject() {
        Optional<Subject> result = currentSubjectId.flatMap(this::findSubject);
        return result;
    }

    public long getVersion() {
        return version;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public Optional<String> getCurrentSubjectId() {
        return currentSubjectId;
    }

//    default void check() {
//        Preconditions.checkState(!currentSubjectId().isPresent() || subjects().containsKey(currentSubjectId().get()), "Le sujet en cours n'existe pas");
//    }

}
