package org.chorem.lrcec.service;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableMySubject.class)
public interface MySubject {

    long duration();

    Feeling feeling();

    Vote vote();

    Subject subject();

}
