package org.chorem.lrcec.service;

public enum Vote {
    NPPPAV,
    Abstention,
    Contre,
    Pour
}
