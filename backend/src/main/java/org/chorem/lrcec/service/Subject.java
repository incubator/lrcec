package org.chorem.lrcec.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class Subject {

    String id;

    LocalDateTime startedAt = LocalDateTime.now();

    Optional<LocalDateTime> finishedAt = Optional.empty();

    String title;

    Map<String, Feeling> feelings = new LinkedHashMap<>();

    Map<String, Vote> votes = new LinkedHashMap<>();

    int bullshitLevel = 0;

    Map<Feeling, Integer> feelingsCount = new LinkedHashMap<>();

    Map<Vote, Integer> votesCount = new LinkedHashMap<>();

    public String getId() {
        return id;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public Optional<LocalDateTime> getFinishedAt() {
        return finishedAt;
    }

    public String getTitle() {
        return title;
    }

    public Map<String, Feeling> getFeelings() {
        return feelings;
    }

    public Map<String, Vote> getVotes() {
        return votes;
    }

    public int getBullshitLevel() {
        return bullshitLevel;
    }

    public Map<Vote, Integer> getVotesCount() {
        return votesCount;
    }

    public Map<Feeling, Integer> getFeelingsCount() {
        return feelingsCount;
    }

    public long getRealTimeDuration() {
        LocalDateTime end = finishedAt.orElse(LocalDateTime.now());
        Duration duration = Duration.between(startedAt, end);
        return duration.getSeconds();
    }

    int bullshitLevel() {
        if (feelings.isEmpty()) {
            return 0;
        }
        int total = 0;
        for (Feeling f : feelings.values()) {
            total += f.getBullshitLevel();
        }
        int result = total / feelings.size();
        return result;
    }

    Map<Vote, Integer> votesCount() {
        Map<Vote, Integer> result = new LinkedHashMap<>();
        for (Vote vote : Vote.values()) {
            long count = votes.values()
                    .stream()
                    .filter(elem -> elem.equals(vote))
                    .count();
            result.put(vote, (int)count);
        }
        return result;
    }

    Map<Feeling, Integer> feelingsCount() {
        Map<Feeling, Integer> result = new LinkedHashMap<>();
        for (Feeling f : Feeling.values()) {
            long count = feelings.values()
                    .stream()
                    .filter(elem -> elem.equals(f))
                    .count();
            result.put(f, (int)count);
        }
        return result;
    }

    public void computeValues() {
        this.bullshitLevel = bullshitLevel();
        this.votesCount = votesCount();
        this.feelingsCount = feelingsCount();
    }

    MySubject forMe(String userId) {
        // On commence par vérifier s'il est nécessaire d'initialiser
        boolean isDirty = false;
        if (!getFeelings().containsKey(userId)) {
            getFeelings().put(userId, Feeling.CaMeVa);
            isDirty = true;
        }
        if (!getVotes().containsKey(userId)) {
            getVotes().put(userId, Vote.NPPPAV);
            isDirty = true;
        }
        if (isDirty) {
            this.computeValues();
        }

        ImmutableMySubject.Builder builder = ImmutableMySubject.builder();
        builder.duration(getRealTimeDuration());
        Feeling feeling = feelings.get(userId);
        builder.feeling(feeling);
        Vote vote = votes.get(userId);
        builder.vote(vote);
        builder.subject(this);
        ImmutableMySubject result = builder.build();
        return result;
    }
}
