package org.chorem.lrcec.service;

public enum Feeling {
    CaMeVa(0),
    JeDecroche(15),
    JeSuisMalALAise(50),
    JeMEmmerde_CaTraine(75),
    CaMEnerve_Foutaises(100);

    int bullshitLevel;

    Feeling(int bullshitLevel) {
        this.bullshitLevel = bullshitLevel;
    }

    public int getBullshitLevel() {
        return bullshitLevel;
    }

}
