package org.chorem.lrcec.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/about")
@Produces(MediaType.APPLICATION_JSON)
public class AboutResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String about() {
        return "Oui, Les Réunions C'Est Chiant";
    }

}
