package org.chorem.lrcec.rest;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/api/meeting/{meetingId}/subject/currentWS")
@ApplicationScoped
public class SubjectSwitchWatcher {

    protected Multimap<String, Session> sessions = HashMultimap.create();

    @OnOpen
    public void onOpen(Session session, @PathParam("meetingId") String meetingId) {
        System.out.println("New bored people in meeting " + meetingId);
        sessions.put(meetingId, session);
        System.out.println(sessions.get(meetingId).size() + " people sleeping in meeting " + meetingId);
    }

    @OnClose
    public void onClose(Session session, @PathParam("meetingId") String meetingId) {
        System.out.println("Too bored to stay in meeting " + meetingId);
        sessions.remove(meetingId, session);
        System.out.println(sessions.get(meetingId).size() + " people still boring in meeting " + meetingId);
    }

    @OnError
    public void onError(Session session, @PathParam("meetingId") String meetingId, Throwable throwable) {
        sessions.remove(meetingId, session);
    }

    public void switchSubject(String meetingId, String subjectId) {
        sessions.get(meetingId).forEach(s -> {
            s.getAsyncRemote().sendObject(subjectId, result ->  {
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            });
        });
    }

}
