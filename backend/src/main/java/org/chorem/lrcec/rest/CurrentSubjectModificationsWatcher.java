package org.chorem.lrcec.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.chorem.lrcec.service.Subject;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/api/meeting/{meetingId}/watcherWS")
@ApplicationScoped
public class CurrentSubjectModificationsWatcher {

    protected Multimap<String, Session> sessions = HashMultimap.create();

    @Inject
    protected ObjectMapper mapper;

    @OnOpen
    public void onOpen(Session session, @PathParam("meetingId") String meetingId) {
        System.out.println("New watcher for meeting " + meetingId);
        sessions.put(meetingId, session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("meetingId") String meetingId) {
        System.out.println("Watcher leaving meeting " + meetingId);
        sessions.remove(meetingId, session);
    }

    @OnError
    public void onError(Session session, @PathParam("meetingId") String meetingId, Throwable throwable) {
        sessions.remove(meetingId, session);
    }

    public void subjectUpdated(String meetingId, Subject currentSubject) {
        try {
            String currentSubjectJson = mapper.writeValueAsString(currentSubject);
            sessions.get(meetingId).forEach(s -> {
                s.getAsyncRemote().sendObject(currentSubjectJson, result ->  {
                    if (result.getException() != null) {
                        System.out.println("Unable to send message: " + result.getException());
                    }
                });
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
