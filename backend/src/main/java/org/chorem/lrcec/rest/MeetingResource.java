package org.chorem.lrcec.rest;

import org.chorem.lrcec.service.Feeling;
import org.chorem.lrcec.service.Meeting;
import org.chorem.lrcec.service.MeetingService;
import org.chorem.lrcec.service.MySubject;
import org.chorem.lrcec.service.Subject;
import org.chorem.lrcec.service.Vote;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Path("/api/meeting")
@Produces(MediaType.APPLICATION_JSON)
public class MeetingResource {

    @Inject
    protected MeetingService service;

    @Inject
    protected SubjectSwitchWatcher subjectSwitchWatcher;

    @Inject
    protected CurrentSubjectModificationsWatcher currentSubjectModificationsWatcher;

    @POST
    public Response create() {
        String id = service.create();
        // XXX AThimel 25/08/2020 C'est nul comme fausse sérialisation à la main mais sinon ce n'est pas considéré comme JSON valide
        Response response = Response.ok(String.format("\"%s\"", id)).build();
        return response;
    }

    @GET
    @Path("/{meetingId}")
    public Response getMeeting(@PathParam("meetingId") String meetingId) {
        Meeting meeting = service.getMeeting(meetingId);
        if (meeting == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        Response response = Response.ok(meeting).build();
        return response;
    }

    @POST
    @Path("/{meetingId}/subject")
    @Consumes(MediaType.TEXT_PLAIN)
    public String createSubject(@PathParam("meetingId") String meetingId, String title) {
        String id = service.newSubject(meetingId, title);
        subjectSwitchWatcher.switchSubject(meetingId, id);
        return id;
    }

    @GET
    @Path("/{meetingId}/subject/current")
    public MySubject getCurrentSubject(@PathParam("meetingId") String meetingId, @Context HttpServletRequest request) {
        String userId = request.getSession(true).getId();
        MySubject result = service.getCurrentSubjectForMe(meetingId, userId);
        return result;
    }

    @GET
    @Path("/{meetingId}/subject/{subjectId}")
    public Response getSubject(@PathParam("meetingId") String meetingId, @PathParam("subjectId") String subjectId, @Context HttpServletRequest request) {
        String userId = request.getSession(true).getId();
        MySubject result = service.getSubject(meetingId, subjectId, userId);
        if (result == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        Response response = Response.ok(result).build();
        return response;
    }

    @POST
    @Path("/{meetingId}/subject/{subjectId}/vote")
    @Consumes(MediaType.TEXT_PLAIN)
    public void vote(@PathParam("meetingId") String meetingId, @PathParam("subjectId") String subjectId, Vote vote, @Context HttpServletRequest request) {
        String userId = request.getSession(true).getId();
        service.newVote(meetingId, subjectId, vote, userId);
        Subject currentSubject = service.getCurrentSubject(meetingId);
        if (currentSubject.getId().equals(subjectId)) {
            currentSubjectModificationsWatcher.subjectUpdated(meetingId, currentSubject);
        }
    }

    @POST
    @Path("/{meetingId}/subject/{subjectId}/feeling")
    @Consumes(MediaType.TEXT_PLAIN)
    public void feeling(@PathParam("meetingId") String meetingId, @PathParam("subjectId") String subjectId, Feeling feeling, @Context HttpServletRequest request) {
        String userId = request.getSession(true).getId();
        service.newFeeling(meetingId, subjectId, feeling, userId);
        Subject currentSubject = service.getCurrentSubject(meetingId);
        if (currentSubject.getId().equals(subjectId)) {
            currentSubjectModificationsWatcher.subjectUpdated(meetingId, currentSubject);
        }
    }

    @GET
    public Map<String, Meeting> list() {
        Map<String, Meeting> result = service.list();
        return result;
    }

    @GET
    @Path("/possibleFeelings")
    public List<Feeling> possibleFeelings() {
        return Arrays.asList(Feeling.values());
    }

    @GET
    @Path("/possibleVotes")
    public List<Vote> possibleVotes() {
        return Arrays.asList(Vote.values());
    }

}
