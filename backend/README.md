# Construction du backend

## Démarrage du projet avec [Quarkus](https://quarkus.io/) en mode 'dev'

    mvn clean compile quarkus:dev

L'application est ensuite disponible sur http://localhost:8080

## Compilation pour une mise en prod

    mvn clean package

Il est ensuite possible de lancer l'application via la commande

    java -jar target/lrcec-*-runner.jar

L'application est ensuite disponible sur http://localhost:8080

## Compilation pour une mise en prod (natif)

Il faut d'abord installer [GraalVM](https://www.graalvm.org/) :
  * Télécharger l'archive sur le site puis la décompresser
  * Définir la variable d'environnement `GRAALVM_HOME`
  * Installer la `native-image` via la commande : `${GRAALVM_HOME}/bin/gu install native-image`

Complication Java + native :

    mvn clean package -Pnative

Il est ensuite possible de lancer l'application via la commande

    ./target/lrcec-*-runner

L'application est ensuite disponible sur http://localhost:8080

Plus d'information sur la compilation native [sur le site de Quarkus](https://quarkus.io/guides/building-native-image)

## Comparaison des temps de démarrage

Pour ce que ça vaut ...
  * dev : 2.4s
  * prod (JVM) : 1.9s
  * prod (natif) : 0.01s

# Images Docker

## Docker JVM

Construction ...

    docker build -f src/main/docker/Dockerfile.jvm -t chorem/lrcec-jvm .

Démarage

    docker run -i --rm -p 8080:8080 chorem/lrcec-jvm

## Docker natif

Construction ...

    docker build -f src/main/docker/Dockerfile.native -t chorem/lrcec .

Démarage

    docker run -i --rm -p 8080:8080 chorem/lrcec

