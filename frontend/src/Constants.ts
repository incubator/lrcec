export default class Constants {
    static baseApiUrl ():string {
        let result = process.env.VUE_APP_API_URL;
        if (!result) {
            result = location.protocol + "//" + location.hostname;
            if (process.env.VUE_APP_API_DEFAULT_PORT) {
                result += ":" + process.env.VUE_APP_API_DEFAULT_PORT
            }
            result += "/api";
        }
        return result;
    }

    static apiUrl (path:string):string {
        const result = Constants.baseApiUrl() + path;
        return result;
    }

    static wsBaseApiUrl ():string {
        let result = Constants.baseApiUrl();
        if (result.indexOf('https://') === 0) {
            result = "wss://" + result.substring(8);
        } if (result.indexOf('http://') === 0) {
            result = "ws://" + result.substring(7);
        } else {
            console.warn("Pas cool comme baseUrl", result);
        }
        return result;
    }

    static wsApiUrl (path:string):string {
        const result = Constants.wsBaseApiUrl() + path;
        return result;
    }
}
