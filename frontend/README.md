# frontend

## Installation des dépendances
```
npm install
```

## Démarrage pour le développement (port 8081)
```
npm run serve
```

## Compilation pour la production (dossier de sortie : `dist`)
```
npm run build
```

## Création de l'APK Android (après `npm run build`)
```
npx cap copy
npx cap sync
cd android
./gradlew clean
./gradlew assemble
```

L'APK est ensuite dispo dans `android/app/build/outputs/apk/[debug|assemble]`
